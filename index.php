<?php

$firstName = 'Matty';
$lastName = 'Jamson';
$position = 'UX/UI Designer';

$title = [
       ['title'=> 'About Me','description'=> '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis volutpat nec tortor a eleifend. Proin nec ligula vel sem luctus porttitor. Cras at interdum libero. Donec nec mauris velit.</p><p>Vestibulum eu eros tortor. Aliquam cursus nunc mauris, nec congue tortor pretium et. Pellentesque feugiat justo in metus laoreet consectetur. Mauris at tempor ipsum, sit amet etae posuere. Nunc auctor sollicitudin sem ut molestie. Pellentesque ligula sapien, ullamcorper et tempor id, congue ac sapien.</p>'],
];
//foreach ($title as $aboutMe){
    //echo $aboutMe['title'];
    //echo $aboutMe['Description'];
//}


$title2 =  'Personal Skills';
$description2 = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis volutpat nec.';


$personalSkills = [
        ['title' => 'Communication', 'level' => '85'],
        ['title'=> 'Team work', 'level'=> '80'],
        ['title'=> 'Leadership', 'level' => '95' ],
        ['title'=> 'Management', 'level'=> '85'],
        ];
//for ($index=0,$counter=count($personalSkills);$index< $counter;$index++){
//echo $personalSkills [$index]['title'];
//echo $personalSkills [$index]['level'];
//};
$title3 = 'Professional Skills';
$description3 = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis volutpat nec.';
$professionalSkills = [
        ['title'=> 'Create Wireframe', 'level'=> '9' ],
        ['title'=> 'Axure', 'level'=> '8'],
        ['title'=> 'Illustrator', 'level'=> '9' ],
        ['title'=> 'Photoshop', 'level'=> '7'],
        ['title'=> 'Digital Marketing', 'level'=> '4'],
];

$workExperience = [
            ['start_date'=> 'JANUARY, 2013',
            'end_date'=> 'PRESENT',
            'title'=> 'Flash media inc.',
            'position'=> 'SENIOR UX/UI DESIGNER update',
            'description'=> 'Lorem ipsum dolor suit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem.',
            ],
         ['start_date'=> 'MARCH,2011',
            'end_date'=> 'DECEMBER,2012',
            'title'=> 'Codedash Studio',
            'position'=> 'UX DESIGNER',
            'description'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem.',
         ],
        ['start_date'=> 'JULY,2009',
            'end_date'=> 'APRIL,2011',
            'title'=> 'Foursqure Studio',
            'position'=> 'VISUAL / UI DESIGNER ',
            'description'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem.',
        ],
];
$education = [
            ['date'=> 'JANUARY, 2007',
                'title'=> 'Master Degree Of Design.',
                'designation'=> 'UNIVERSITY OF DESIGN',
                'description'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem.',
    ],

            ['date'=> 'JUNE, 2004',
             'title'=> 'Bachelor Of Arts',
             'designation'=> 'UNIVERSITY OF DESIGN',
              'description'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem.',
    ],
    ['date'=> 'JANUARY, 2001',
        'title'=> 'Master Degree Of Design',
        'designation'=> 'UNIVERSITY OF DESIGN',
        'description'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem.',
    ],
];

$references= [
        [   'description'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem.',
            'name'=> 'Jonathan doe',
            'position'=> 'PROJECT MANAGER, MATRIX MEDIA',
],
            [   'description'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem.',
                'name'=> 'Jonathan doe',
                'position'=> 'PROJECT MANAGER, MATRIX MEDIA',
    ],
    [   'description'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem.',
        'name'=> 'Jonathan doe',
        'position'=> 'PROJECT MANAGER, MATRIX MEDIA',
    ],
    [   'description'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit maxime laborum sequi, magni earum quo soluta sint velit numquam, ipsum illum! Nostrum possimus illo architecto praesentium ut aliquam dolorem.',
        'name'=> 'Jonathan doe',
        'position'=> 'PROJECT MANAGER, MATRIX MEDIA',
    ],
];

$links= [
        [   'circl_class' => 'google-plus color-white',
            'icon_class' => 'fab fa-google-plus-g',
            'link'=> 'https://www.google.com/',
        ],
    ['circl_class' => 'facebook',
        'icon_class' => 'fab fa-facebook-f',
        'link'=> 'https://www.facebook.com',
        ],
['circl_class' => 'dribbble',
    'icon_class' => 'fab fa-dribbble',
    'link'=> 'https://dribbble.com',
    ] ,

    ['circl_class' => 'behance',
        'icon_class' => 'fab fa-behance',
        'link'=> 'https://www.behance.net',
    ],
    ['circl_class' => 'linkdin',
    'icon_class' => 'fab fa-linkedin-in',
    'link'=> 'https://www.linkedin.com',
],
    ['circl_class' => 'twitter',
        'icon_class' => 'fab fa-twitter',
        'link'=> 'https://twitter.com',
    ],
    ['circl_class' => 'instagram',
        'icon_class' => 'fab fa-instagram',
        'link'=> 'https://www.instagram.com',
    ],
    ['circl_class' => 'vimeo',
        'icon_class' => 'fab fa-vimeo-v',
        'link'=> 'https://vimeo.com',
    ],
];
$portfolio_Title= 'PORTFOLIO';
$current= 'All';
$branding= 'Branding';
$illustration= 'Illustration';
$wordpress= 'Wordpress';
$site_template= 'Site Template';

$portfolio= [
        ['data-gal'=> 'prettyPhoto[gallery1]',
            'title'=> 'Project Name 1',
            'link'=> 'http://via.placeholder.com/800x600',
            'class'=> 'fa fa-image',
],
    ['data-gal'=> 'prettyPhoto[gallery1]',
        'title'=> 'Project Name 2',
        'link'=> 'http://via.placeholder.com/800x600',
        'class'=> 'fa fa-image',
    ],
    ['data-gal'=> 'prettyPhoto[gallery1]',
        'title'=> 'Project Name 3',
        'link'=> 'http://via.placeholder.com/400x300',
        'class'=> 'fa fa-image',
    ],
    ['data-gal'=> 'prettyPhoto[gallery1]',
        'title'=> 'Project Name 4',
        'link'=> 'http://via.placeholder.com/800x600',
        'class'=> 'fa fa-image',
    ],
    ['data-gal'=> 'prettyPhoto[gallery1]',
        'title'=> 'Project Name 5',
        'link'=> 'http://via.placeholder.com/800x600',
        'class'=> 'fa fa-image',
    ],
    ['data-gal'=> 'prettyPhoto[gallery1]',
        'title'=> 'Project Name 6',
        'link'=> 'https://vimeo.com/244717336',
        'class'=> 'fa fa-video-camera',
    ],
    ['data-gal'=> 'prettyPhoto[gallery1]',
        'title'=> 'Project Name 7',
        'link'=> 'https://www.youtube.com/watch?v=0vrdgDdPApQ',
        'class'=> 'fa fa-video-camera',
    ],
    ['data-gal'=> 'prettyPhoto[gallery1]',
        'title'=> 'Project Name 8',
        'link'=> 'videos/movie.mp4',
        'class'=> 'fa fa-video-camera',
    ],
];


?><!DOCTYPE html>
<html lang="zxx">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="Responsive Landing Page for Presenting Your Resume">
      <meta name="keywords" content="one page, responsive, html template, responsive landing page">
      <meta name="author" content="ThinkingCoder">
      <title>Flatr - vCard, CV, Resume & Portfolio template</title>
      <!-- FAVICON  -->
      <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
      <link rel="icon" href="favicon.ico" type="image/x-icon">
      <!-- Custom Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800,900" rel="stylesheet">
      <!-- Bootstrap Core CSS -->
      <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="lib/bootstrap/css/normalize.css" rel="stylesheet">
      <!-- Plugin CSS -->
      <link rel="stylesheet" href="lib/font-awesome/css/all.css">
      <link rel="stylesheet" href="lib/wowjs/animate.min.css">
      <link rel="stylesheet" href="lib/pretty-photo/prettyPhoto.css">
      <!-- Template CSS -->
      <link rel="stylesheet" id="colors" href="css/layout1-color1.css" type="text/css">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <!-- =========================================
         Navigation
         ========================================== -->
      <!--start mobile navigation-->
      <div class="nav visible-xs fixed-top">
         <div class="container">
            <div class="row">
               <div class="col-sm-12">
                  <button class="btn btn-outline inverse btn-sm navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">Menu
                  <i class="fa fa-bars ico-sm"></i></button>
               </div>
               <div class="collapse navbar-collapse" id="navbarResponsive">
                  <ul class="navbar-nav text-uppercase">
                     <li class="page-scroll nav-item">
                        <a class="nav-link scroll-to" data-toggle="collapse" data-target="#navbarResponsive" href="#introduction">introduction</a>
                     </li>
                     <li class="page-scroll nav-item">
                        <a class="nav-link scroll-to" data-toggle="collapse" data-target="#navbarResponsive" href="#skillset">skillset</a>
                     </li>
                     <li class="page-scroll nav-item">
                        <a class="nav-link scroll-to" data-toggle="collapse" data-target="#navbarResponsive" href="#experience">experience</a>
                     </li>
                     <li class="page-scroll nav-item">
                        <a class="nav-link scroll-to" data-toggle="collapse" data-target="#navbarResponsive" href="#portfolio">portfolio</a>
                     </li>
                     <li class="page-scroll nav-item">
                        <a class="nav-link scroll-to" data-toggle="collapse" data-target="#navbarResponsive" href="#references">references</a>
                     </li>
                     <li class="page-scroll nav-item">
                        <a class="nav-link scroll-to" data-toggle="collapse" data-target="#navbarResponsive" href="#connect">connect</a>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--end mobile navigation-->
      <nav class="nav-container hidden-xs">
         <ul class="nav">
            <li class="page-scroll">
               <a href="#introduction">introduction</a>
            </li>
            <li class="page-scroll">
               <a href="#skillset">skills</a>
            </li>
            <li class="page-scroll">
               <a href="#experience">experience</a>
            </li>
            <li class="page-scroll">
               <a href="#portfolio">portfolio</a>
            </li>
            <li class="page-scroll">
               <a href="#references">references</a>
            </li>
            <li class="page-scroll">
               <a href="#connect">connect</a>
            </li>
         </ul>
      </nav>
      <!-- =========================================
         Main Container
         ========================================== -->
      <div class="container main-container">
         <div class="row">
            <!-- =========================================
               HEADER
               ========================================== -->
            <header>
               <div class="col-md-12 extra-offset-md">
                  <button class="btn btn-transparent">
                     <svg>
                        <use xlink:href="img/svg/icons.svg#download"></use>
                     </svg>
                     Download resume
                  </button>
                  <div class="resume-title">
                     <h2><?= $firstName ?></h2>
                     <h2><?= $lastName ?></h2>
                     <div class="resume-designation extra-offset-md">
                        <span class="border"></span>
                        <span><?= $position ?></span>
                     </div>
                  </div>
               </div>
               <div class="header-overlay"></div>
            </header>
            <!-- =========================================
               ABOUT ME
               ========================================== -->

            <section id="introduction">
                <?php foreach ($title as $aboutMe):?>
               <div class="col-md-offset-1 col-md-10">
                  <h2 class="title"><?= $aboutMe ['title']?></h2>
                  <hr>
                   <?= $aboutMe ['description'] ?>
               </div>
                <?php endforeach?>
            </section>
            <!-- =========================================
               SKILL
               ========================================== -->
            <section id="skillset">
               <div class="col-md-offset-1 col-md-5 offset-lt">
                  <h2 class="title"> <?= $title2 ?></h2>
                  <hr>
                  <p> <?= $description2?></p>
                  <div class="skill-wrapper">
                     <!--skill-->
                     <ul class="skill" id="skills">
                         <?php for ($index=0,$counter=count($personalSkills);$index< $counter;$index++):?>
                        <li>
                           <p><?= $personalSkills [$index]['title'] ?></p>
                           <div class="skill-bar wow slideInLeft" data-width="<?= $personalSkills [$index]['level'] ?>">
                              <span class="skill-tip"></span>
                           </div>
                        </li>
                         <?php endfor; ?>
                     </ul>
                  </div>
               </div>
               <div class="col-md-5 offset-rt">
                  <h2 class="title"><?= $title3 ?>></h2>
                  <hr>
                  <p><?= $description3 ?>></p>
                  <ul class="skill-dots" id="skill-dots">
                      <?php foreach ($professionalSkills as $skill):?>
                     <li class="skill">
                        <span class="skill-title"><?= $skill['title']?></span>
                        <div class="skill-progress" data-score="<?= $skill['level'] ?>"></div>
                     </li>
                      <?php endforeach;?>
                  </ul>
               </div>
            </section>
            <!-- =========================================
               WORK
               ========================================== -->
            <section id="experience">
               <div class="col-md-offset-1 col-md-5  offset-lt">
                  <h2 class="title">WORK EXPERIENCE</h2>
                  <hr>
                  <div class="timeline-centered">
                     <article class="timeline-entry">
                        <div class="timeline-entry-inner">
                           <div class="timeline-icon"></div>
                           <div class="timeline-label">
                              <?= $workExperience [0]['start_date'] ?> - <?= $workExperience [0]['end_date'] ?>
                           </div>
                           <h3 class="company"><?= $workExperience [0]['title'] ?></h3>
                           <p class="designation"><?= $workExperience [0]['position'] ?> </p>
                           <p class="description"><?= $workExperience [0]['description'] ?></p>
                        </div>
                     </article>
                     <article class="timeline-entry">
                        <div class="timeline-entry-inner">
                           <div class="timeline-icon"></div>
                           <div class="timeline-label">
                               <?= $workExperience [1]['start_date'] ?> - <?= $workExperience [1]['end_date'] ?>
                           </div>
                           <h3 class="company"><?= $workExperience [1]['title'] ?></h3>
                           <p class="designation"><?= $workExperience [1]['position'] ?></p>
                           <p class="description"><?= $workExperience [1]['description'] ?></p>
                        </div>
                     </article>
                     <article class="timeline-entry">
                        <div class="timeline-entry-inner">
                           <div class="timeline-icon"></div>
                           <div class="timeline-label">
                               <?= $workExperience [2]['start_date'] ?> - <?= $workExperience [2]['end_date'] ?>
                           </div>
                           <h3 class="company">F<?= $workExperience [2]['title'] ?></h3>
                           <p class="designation"><?= $workExperience [2]['position'] ?></p>
                           <p class="description"><?= $workExperience [2]['description'] ?></p>
                        </div>
                     </article>
                  </div>
               </div>
               <div class="col-md-5  offset-rt">
                  <h2 class="title">EDUCATION & DIPLOMAS</h2>
                  <hr>
                  <div class="timeline-centered">
                     <article class="timeline-entry">
                        <div class="timeline-entry-inner">
                           <div class="timeline-icon"></div>
                           <div class="timeline-label">
                              <?= $education [0]['date'] ?>
                           </div>
                           <h3 class="company"><?= $education [0]['title'] ?></h3>
                           <p class="designation"><?= $education [0]['designation'] ?></p>
                           <p class="description"><?= $education [0]['description'] ?></p>
                        </div>
                     </article>
                     <article class="timeline-entry">
                        <div class="timeline-entry-inner">
                           <div class="timeline-icon"></div>
                           <div class="timeline-label">
                               <?= $education [1]['date'] ?>
                           </div>
                           <h3 class="company"><?= $education [1]['title'] ?></h3>
                           <p class="designation"><?= $education [1]['designation'] ?></p>
                           <p class="description"><?= $education [1]['description'] ?></p>
                        </div>
                     </article>
                     <article class="timeline-entry">
                        <div class="timeline-entry-inner">
                           <div class="timeline-icon"></div>
                           <div class="timeline-label">
                               <?= $education [2]['date'] ?>
                           </div>
                           <h3 class="company"><?= $education [2]['title'] ?></h3>
                           <p class="designation"><?= $education [2]['designation'] ?> </p>
                           <p class="description"><?= $education [2]['description'] ?></p>
                        </div>
                     </article>
                  </div>
               </div>
            </section>
            <!-- =========================================
               PORTFOLIO
               ========================================== -->
            <section id="portfolio">
               <div class="col-md-offset-1 col-md-10">
                  <h2 class="title"><?=$portfolio_Title?></h2>
                  <hr>
                  <div class="clearfix"></div>
                  <div class="porfolio-wrapper">
                     <div class="portfolioFilter">
                        <ul>
                           <li>
                              <a href="#" data-filter="*"  class="current"><?=$current?></a>
                           </li>
                           <li>
                              <a href="#" data-filter=".branding"><?=$branding?></a>
                           </li>
                           <li>
                              <a href="#" data-filter=".illustration"><?=$illustration?></a>
                           </li>
                           <li>
                              <a href="#" data-filter=".wordpress"><?=$wordpress?></a>
                           </li>
                           <li>
                              <a href="#" data-filter=".site-template"><?=$site_template?></a>
                           </li>
                        </ul>
                     </div>
                     <ul class="portfolioContainer gallery">
                        <li class="wordpress item">
                           <div class="lightCon">
                              <div class="hoverBox">
                                 <ul class="hover-box-inner">
                                    <li>
                                       <a data-gal="<?=$portfolio[0]['data-gal']?>" title="<?=$portfolio[0]['title']?>" href="<?=$portfolio[0]['link']?>"><i class="<?=$portfolio [0]['class']?>"></i></a>
                                    </li>
                                    <li>
                                       <h4>Project Name</h4>
                                    </li>
                                 </ul>
                              </div>
                              <img src="http://via.placeholder.com/300x150" alt="">
                           </div>
                        </li>
                        <li class="branding illustration item">
                           <div class="lightCon">
                              <div class="hoverBox">
                                 <ul class="hover-box-inner">
                                    <li>
                                       <a data-gal="<?=$portfolio[1]['data-gal']?>" title="<?=$portfolio[1]['title']?>" href="<?=$portfolio[1]['link']?>"><i class="<?=$portfolio [1]['class']?>"></i></a>
                                    </li>
                                    <li>
                                       <h4>Project Name</h4>
                                    </li>
                                 </ul>
                              </div>
                              <img src="http://via.placeholder.com/300x150" alt="">
                           </div>
                        </li>
                        <li class="illustration item">
                           <div class="lightCon">
                              <div class="hoverBox">
                                 <ul class="hover-box-inner">
                                    <li>
                                       <a data-gal="<?=$portfolio[2]['data-gal']?>" title="<?=$portfolio[2]['title']?>" href="<?=$portfolio[2]['link']?>"><i class="<?=$portfolio [2]['class']?>"></i></a>
                                    </li>
                                    <li>
                                       <h4>Project Name</h4>
                                    </li>
                                 </ul>
                              </div>
                              <img src="http://via.placeholder.com/300x150" alt="">
                           </div>
                        </li>
                        <li class="branding illustration item">
                           <div class="lightCon">
                              <div class="hoverBox">
                                 <ul class="hover-box-inner">
                                    <li>
                                       <a data-gal="<?=$portfolio[3]['data-gal']?>" title="<?=$portfolio[3]['title']?>" href="<?=$portfolio[3]['link']?>"><i class="<?=$portfolio [3]['class']?>"></i></a>
                                    </li>
                                    <li>
                                       <h4>Project Name</h4>
                                    </li>
                                 </ul>
                              </div>
                              <img src="http://via.placeholder.com/300x150" alt="">
                           </div>
                        </li>
                        <li class="illustration photos item">
                           <div class="lightCon">
                              <div class="hoverBox">
                                 <ul class="hover-box-inner">
                                    <li>
                                       <a data-gal="<?=$portfolio[4]['data-gal']?>" title="<?=$portfolio[4]['title']?>" href="<?=$portfolio[4]['link']?>"><i class="<?=$portfolio [4]['class']?>"></i></a>
                                    </li>
                                    <li>
                                       <h4>Project Name</h4>
                                    </li>
                                 </ul>
                              </div>
                              <img src="http://via.placeholder.com/300x150" alt="">
                           </div>
                        </li>
                        <li class="branding photos item">
                           <div class="lightCon">
                              <div class="hoverBox">
                                 <ul class="hover-box-inner">
                                    <li>
                                       <a data-gal="<?=$portfolio [5]['data-gal']?>" title="<?=$portfolio [5]['title']?>" href="<?=$portfolio [5]['link']?>"><i class="<?=$portfolio [5]['class']?>"></i></a>
                                    </li>
                                    <li>
                                       <h4>Project Name</h4>
                                    </li>
                                 </ul>
                              </div>
                              <img src="http://via.placeholder.com/300x150" alt="">
                           </div>
                        </li>
                        <li class="illustration photos item">
                           <div class="lightCon">
                              <div class="hoverBox">
                                 <ul class="hover-box-inner">
                                    <li>
                                       <a data-gal="<?=$portfolio [6]['data-gal']?>" title="<?=$portfolio [6]['title']?>" href="<?=$portfolio [6]['link']?>"><i class="<?=$portfolio [6]['class']?>"></i></a>
                                    </li>
                                    <li>
                                       <h4>Project Name</h4>
                                    </li>
                                 </ul>
                              </div>
                              <img src="http://via.placeholder.com/300x150" alt="">
                           </div>
                        </li>
                        <li class="site-template item">
                           <div class="lightCon">
                              <div class="hoverBox">
                                 <ul class="hover-box-inner">
                                    <li>
                                       <a data-gal="<?=$portfolio [7]['data-gal']?>" title="<?=$portfolio [7]['title']?>" href="<?=$portfolio [7]['link']?>"><i class="<?=$portfolio [7]['class']?>"></i></a>
                                    </li>
                                    <li>
                                       <h4>Project Name</h4>
                                    </li>
                                 </ul>
                              </div>
                              <img src="http://via.placeholder.com/300x150" alt="">
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
            </section>
            <!-- =========================================
               REFERENCES
               ========================================== -->
            <section id="references" class="testimonial-outer">
               <div class="col-md-offset-1 col-md-10">
                  <h2 class="title">REFERENCES</h2>
                  <hr>
               </div>
               <div class="clearfix"></div>
               <div class="testimonial-wrapper">
                  <div class="col-md-offset-1 col-md-5 offset-lt">
                     <!--testimonial-block-->
                     <div class="testimonial">
                        <div class="testimonialblock">
                           <p>“ <?= $references [0]['description'] ?>”</p>
                        </div>
                        <div class="user-wrapper">
                           <img src="http://via.placeholder.com/100x100" alt="">
                           <div class="user-detail">
                              <p class="title"><?= $references [0]['name'] ?></p>
                              <p><?= $references [0]['position'] ?></p>
                           </div>
                        </div>
                     </div>
                     <!--testimonial-block ends-->
                  </div>
                  <div class="col-md-5 offset-rt">
                     <!--testimonial-block-->
                     <div class="testimonial">
                        <div class="testimonialblock">
                           <p>“<?= $references [1]['description'] ?>”</p>
                        </div>
                        <div class="user-wrapper">
                           <img src="http://via.placeholder.com/100x100" alt="">
                           <div class="user-detail">
                              <p class="title"><?= $references [1]['name'] ?></p>
                              <p><?= $references [1]['position'] ?></p>
                           </div>
                        </div>
                     </div>
                     <!--testimonial-block ends-->
                  </div>
               </div>
               <div class="testimonial-wrapper">
                  <div class="col-md-offset-1 col-md-5 offset-lt">
                     <!--testimonial-block-->
                     <div class="testimonial">
                        <div class="testimonialblock">
                           <p>“<?= $references [2]['description'] ?>”</p>
                        </div>
                        <div class="user-wrapper">
                           <img src="http://via.placeholder.com/100x100" alt="">
                           <div class="user-detail">
                              <p class="title"><?= $references [2]['name'] ?></p>
                              <p><?= $references [2]['position'] ?></p>
                           </div>
                        </div>
                     </div>
                     <!--testimonial-block ends-->
                  </div>
                  <div class="col-md-5 offset-rt">
                     <!--testimonial-block-->
                     <div class="testimonial">
                        <div class="testimonialblock">
                           <p>“<?= $references [3]['description'] ?>”</p>
                        </div>
                        <div class="user-wrapper">
                           <img src="http://via.placeholder.com/100x100" alt="">
                           <div class="user-detail">
                              <p class="title"><?= $references [3]['name'] ?></p>
                              <p><?= $references [3]['position'] ?></p>
                           </div>
                        </div>
                     </div>
                     <!--testimonial-block ends-->
                  </div>
               </div>
            </section>
            <!-- =========================================
               CONNECT ME
               ========================================== -->
            <section id="connect" class="connect padding-bottom-0">
               <div class="col-md-offset-1 col-md-10">
                  <h2 class="title">CONNECT ME</h2>
                  <ul class="social-links social-border">
                      <li class="<?= $links[0] ['circl_class']?>"><a target="_blank" href="<?= $links[0] ['link']?>"</a><i class="<?= $links[0] ['icon_class']?>" aria-hidden="true"></i></li>
                     <li class="<?= $links[1] ['circl_class']?>"><a target="_blank" href="<?= $links[1] ['link']?>"</a> <i class="<?= $links[1] ['icon_class']?>" aria-hidden="true"></i></li>
                     <li class="<?= $links[2] ['circl_class']?>"><a target="_blank" href="<?= $links[2] ['link']?>"</a><i class="<?= $links[2] ['icon_class']?>" aria-hidden="true"></i></li>
                     <li class="<?= $links[3] ['circl_class']?>"><a target="_blank" href="<?= $links[3] ['link']?>"</a><i class="<?= $links[3] ['icon_class']?>" aria-hidden="true"></i></li>
                     <li class="<?= $links[4] ['circl_class']?>"><a target="_blank" href="<?= $links[4] ['link']?>"</a><i class="<?= $links[4] ['icon_class']?>" aria-hidden="true"></i></li>
                     <li class="<?= $links[5] ['circl_class']?>"><a target="_blank" href="<?= $links[5] ['link']?>"</a><i class="<?= $links[5] ['icon_class']?>"aria-hidden="true"></i></li>
                     <li class="<?= $links[6] ['circl_class']?>"><a target="_blank" href="<?= $links[6] ['link']?>"</a><i class="<?= $links[6] ['icon_class']?>" aria-hidden="true"></i></li>
                     <li class="<?= $links[7] ['circl_class']?>"><a target="_blank" href="<?= $links[7] ['link']?>"</a><i class="<?= $links[7] ['icon_class']?>" aria-hidden="true"></i></li>
                  </ul>
                  <hr>
               </div>
               <div class="map_outer">
                  <div id="map"></div>
                  <!--contact form-->
                  <div class="contact_form col-md-4 col-sm-7 col-xs-11">
                     <p>Drop me a line</p>
                     <div class="alert alert-success hidden" id="successMessage">
                        <strong>Success!</strong> Your message has been sent to us.
                     </div>
                     <div class="alert alert-danger hidden" id="errorMessage">
                        <strong>Error!</strong> There was an error sending your message.
                     </div>
                     <form action="php/submitForm.php" method="POST" name="emailSubmission">
                        <div class="col-md-12 ">
                           <label>Enter name</label>
                           <input type="text" class="minimal" name="name" id="name" placeholder="Enter name">
                        </div>
                        <div class="col-md-12">
                           <label>Enter email</label>
                           <input type="text" class="minimal" name="email" id="email" placeholder="Enter email">
                        </div>
                        <div class="col-md-12 ">
                           <label>Enter Subject</label>
                           <input type="text" class="minimal" name="subject" id="subject" placeholder="Enter subject">
                        </div>
                        <div class="col-md-12">
                           <label>Enter message</label>
                           <textarea class="minimal" name="message" id="message" placeholder="Enter message"></textarea>
                        </div>
                        <div class="col-md-12">
                           <button type="submit" name="submit" class="btn btn-md btn-primary inverse pull-right" value="Submit">Submit</button>
                        </div>
                     </form>
                  </div>
               </div>
            </section>
         </div>
         <!--end of Main container-->
      </div>
      <!-- =========================================
         Footer
         ========================================== -->
      <footer>
         © 2017 Flatos.com All right reserved. 
         <a href="#">Theme Elite production</a>
      </footer>
      <!-- =========================
         PRELOADER
         ============================== -->
      <div class="loader-wrapper">
         <div class="preload"></div>
      </div>
      <!-- =========================================
         Scripts
         ========================================== -->
      <!-- jQuery -->
      <script src="lib/jquery/jquery-3.2.1.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="lib/bootstrap/js/bootstrap.min.js"></script>
      <!-- JQuery Easing -->
      <script src="lib/jquery/jquery.easing.min.js"></script>
      <!-- Retina LIbrary-->
      <script src="lib/retina/retina.min.js"></script>
      <!--Wow -->
      <script src="lib/wowjs/wow.min.js"></script>
      <!-- Isotope Plugin -->
      <script src="lib/isotope/isotope.pkgd.js"></script>
      <!-- Pretty-Photo Gallery -->
      <script src="lib/pretty-photo/jquery.prettyPhoto.min.js"></script>
      <!-- Jquery Validator -->
      <script src='lib/jquery/jquery.validate.min.js'></script>
      <!-- Google Maps -->
      <script src="lib/googlemaps/infobubble.js"></script>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDMQqDC0jmMiZmHAbC_q57Zq3e2obnLMw"></script>
      <script>
         // When the window has finished loading create our google map below
         google.maps.event.addDomListener(window, 'load', init);
         
         function init() {
             // Basic options for a simple Google Map
             // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
             var mapOptions = {
                 // How zoomed in you want the map to start at (always required)
                 zoom: 11,
                 // The latitude and longitude to center the map (always required)
                 center: new google.maps.LatLng(40.702603, -73.788742), // New York
                 // This is where you would paste any style found on Snazzy Maps.
                 styles: [{ "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }]
             };
         
             // Get the HTML DOM element that will contain your map 
             // We are using a div with id="map" seen below in the <body>
             var mapElement = document.getElementById('map');
         
             // Create the Google Map using our element and options defined above
             var map = new google.maps.Map(mapElement, mapOptions);
             var infowindow = new InfoBubble({
                 shadowStyle: 0,
                 padding: 30,
                 backgroundColor: '#fff',
                 borderRadius: 7,
                 arrowSize: 10,
                 borderWidth: 0,
                 maxWidth: 245,
                 borderColor: '#2c2c2c',
                 disableAutoPan: true,
                 hideCloseButton: true,
                 arrowPosition: 30,
                 backgroundClassName: 'transparent',
                 arrowStyle: 0
             });
             var marker = new google.maps.Marker({
                 position: new google.maps.LatLng(40.6700, -73.9400),
                 icon: new google.maps.MarkerImage('img/svg/marker.svg',
                     null, null, null, new google.maps.Size(50, 50)),
                 map: map,
                 title: 'yo'
             });
             google.maps.event.addListener(marker, 'click', (function(marker) {
                 return function() {
                     infowindow.setContent('\u003cp\u003eCollins Street West Victoria 8007 Australia.\u003c/p\u003e\u003cp\u003eCall : +1800 1234 56789\u003c/p\u003e');
                     infowindow.open(map, marker);
                 }
             })(marker));
         }
      </script>
      <!-- Template Custom scripts -->
      <script src="js/scripts.min.js"></script>
   </body>
</html>